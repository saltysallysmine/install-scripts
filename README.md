# Install scripts

This is bash-scripts that do the basic configuration after installing Linux: download useful applications and set keyboard shortcuts.

1. Create new ssh-key and add it to ssh-agent and to GitLab.
2. Clone install-scripts and dotfiles from GitLab.
3. Make all scripts in this folder executable and execute main.
4. Check hotkeys. Some of them may be hurted.
5. Configure timeshift.

